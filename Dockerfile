FROM nginx:1.17.2-alpine

LABEL maintainer="Colin Wilson (colin@aig.is)"
LABEL vendor="AIGIS"

WORKDIR /usr/share/nginx/